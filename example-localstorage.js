(function ($) {

// A simple example to show recording your search history.

/**
 * Save search queries from the search module block and form at node/search.
 */
Drupal.behaviors.saveSearchQueries = {
  attach: function (context, settings) {
    $('#search-block-form, #search-form').not('.save-search-term-processed').each(function() {
      $(this).addClass('save-search-term-processed');
      $(this).submit(function() {
        var searchQueries = $.getSessionItem('searchQueries', new Array());
        var lastSearch = $(this).find('input[type=text]').val();
        if ($.inArray(lastSearch, searchQueries) == -1) {
          searchQueries.unshift(lastSearch);
          if (searchQueries.length > 5) {
            searchQueries.pop();
          }
          $(searchQueries).setSessionItem('searchQueries');
        }
      });
    });
  }
};

/**
 * Show recently searched terms in the search block.
 */
Drupal.behaviors.showSearchQueries = {
  attach: function (context, settings) {
    var searchQueries = $.getSessionItem('searchQueries', new Array());
    if (searchQueries.length < 1) {
      return;
    }

    $('#search-block-form:not(.show-search-term-processed)').each(function() {
      $(this).append('<h2>' + Drupal.t('Recent searches') + '</h2><ul></ul>');
      for (var i = 0; i < searchQueries.length; i++) {
        var searchURL = '<a href="' + Drupal.settings.basePath + 'search/node/' + searchQueries[i] + '">' + searchQueries[i] + '</a>';
        $(this).find('ul').append('<li>' + searchURL + '</li>');
      }
    });
  }
}

/**
 * Helper function to retrieve a JSON-encoded item from sessionStorage.
 *
 * @param key
 *   The key of the item to retrieve.
 * @param def
 *   Return this value as the default value if the key does not exist in the
 *   sessionStorage.
 *
 * @return
 *   The object that was stored.
 */
$.getSessionItem = function(key, def) {
  var json = sessionStorage.getItem(key);
  if (json != null) {
    return JSON.parse(json);
  }
  return def;
};

/**
 * Helper function to save an object to sessionStorage.
 *
 * @param key
 *   The key to save the object under.
 *
 * @return
 *   The object that was saved.
 */
$.fn.setSessionItem = function(key) {
  var json = JSON.stringify(this);
  sessionStorage.setItem(key, json);
  return this;
}

}(jQuery));

